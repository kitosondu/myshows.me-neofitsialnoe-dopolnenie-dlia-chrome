var stats = {

    init: function() {
        utils.translate();
        stats.renderStats();
    },

    renderStats: function() {
        var username = utils.getFromLocalStorage('username'),
            password_md5 = utils.getFromLocalStorage('password_md5');
        $.ajax({
            url: 'https://api.myshows.me/profile/login',
            data: {
                login: username,
                password: password_md5
            },
            success: function(response) {

            },
            statusCode: {
                200: function(xhr) {
                    // logged in
                    $.ajax({
                        url: 'https://api.myshows.me/profile/' + username,
                        type: 'get',
                        success: function(response) {
                            try {
                                response = JSON.parse(response);
                            } catch (err) {
                                response = {};
                            }
                            response = response || {};
                            //console.log(response)
                            if (typeof response.stats == 'undefined' && typeof response.stats.watchedHours == 'undefined') {
                                stats.showError(utils.i18n('stats_not_avail'));
                            } else {

                                // TODO
                                var hoursRu = function(n) {
                                    n = n % 10;
                                    if (n == 1)
                                        return 'час';
                                    else if (n >=2 && n <= 4)
                                        return 'часа';
                                    else 
                                        return 'часов';
                                };
                                var monthsRu = function(n) {
                                    n = n % 10;
                                    if (n == 1)
                                        return 'месяц';
                                    else if (n >=2 && n <= 4)
                                        return 'месяца';
                                    else 
                                        return 'месяцев';
                                };

                                //$('.stats-text').html(utils.i18n('stats_spent_hours', [Math.round(response.stats.watchedHours)]));
                                var h = Math.round(response.stats.watchedHours);
                                $('.stats-text').html('<p>Потрачено <span class="badge">' + h + ' ' + hoursRu(h) + '</span> на сериалы.</p>');

                                var m = Math.round(response.stats.watchedHours / (30 * 24));                         
                                if (m >= 1) {
                                    $('.stats-text').append('<div class="alert alert-info"><span class="fa fa-thumbs-up stats-info-icon"></span> Это примерно <b>' + m + ' ' + monthsRu(m) + '</b> непрерывного просмотра.</div>');
                                }
                            }
                        },
                        error: function(xhr, status, error) {
                            stats.showError(utils.i18n('stats_not_avail'));
                        }
                    });
                },
                403: function(xhr) {
                    // wrong credentials
                    stats.showError(utils.i18n('stats_not_avail'));
                },
                404: function(xhr) {
                    // empty data
                    stats.showError(utils.i18n('stats_not_avail'));
                }
            }

        });

    },

    showError: function(err) {
        $('.error-msg .text').html(err);
        $('.error-msg').fadeIn();
    }
};

$(function(){
    //log('loaded...')

    stats.init();

});
