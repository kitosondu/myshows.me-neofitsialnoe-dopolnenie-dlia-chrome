;(function($) {

    $.myQueue = function(opts) {

        var plugin = this,
            queue = [];

        var defaults = {
            onFinish: null
        };
        opts = opts || {};
        opts = $.extend({}, defaults, opts);
        
        /*
            Note: delay does not work for the first queue item

            @param func - must return Deferred promise obj
        */
        plugin.add = function(func, delayMs, onStart, onExec) {
            delayMs = delayMs || 0;
            onExec = onExec || null;
            onStart = onStart || null;
            queue.push({func: func, delay: delayMs, running: 0, waiting: 0, onStart: onStart, onExec: onExec});
        };

        plugin.length = function() {
            return queue.length;  
        };
        
        /*
            Can be called more than once
        */
        plugin.run = function() {
            if (queue.length == 0 || queue[0].running) {
                if (typeof opts.onFinish == 'function')
                    opts.onFinish();
                return;
            }
            queue[0].running = 1;
            if (typeof queue[0].onStart == 'function')
                queue[0].onStart();
            queue[0].func().always(function(){
                if (typeof queue[0].onExec == 'function')
                    queue[0].onExec();
                if (queue[0].delay) {
                    setTimeout(function(){ queue.shift(); plugin.run(); }, queue[0].delay);
                } else {
                    queue.shift();
                    plugin.run();
                }
            });
        };

        plugin.stop = function() {
            queue = [];
        };
    }

})(jQuery);