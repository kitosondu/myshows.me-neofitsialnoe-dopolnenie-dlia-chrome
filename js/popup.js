var DEBUG = 1;

function dbg(val) {
    $('#dbg').show().find('#output').html(val);
}

var popup = {

    spinner: false,
    msgTimeoutID: false,
    timezone_jstz: '',

    init: function() {

        /*chrome.i18n.getAcceptLanguages(function(langs){
            log(langs);
        });*/

        utils.translate();

        $('body').addClass(utils.getCfg('settings_interface') + '-interface');

        $('#creds_site').on('click', function(){
            chrome.tabs.create({'url': 'https://www.myshows.me'}, function(tab) {
                // Tab opened.
            });     
            return false;       
        });

        $('body').on('click', '#msgbox', function(){
            popup.hideMsg();
        });

        $('#login-form').on('submit', popup.onLoginFormSubmit);

        $('#try_again button').on('click', function(){
            popup.checkLoggedStatus();
            $('#try_again').hide();
        });

        $('.logout-link').on('click', function(e){
            e.preventDefault();
            popup.logout();
        });

        $('.reload-link').on('click', function(e){
            utils.deleteCache();
            popup.loadShowsList();
        });

        /*$('.help-link').on('click', function(e){
            e.preventDefault();
            var elm = $(this);
            utils.showTooltip({
                element: $('.header'),
                position: 'bottom',
                text: utils.i18n('instructions_text'),
                raw: {
                    //position: { my: 'top right', at: 'bottom left', adjust: {x: -5, y: 5} },
                    //position: { adjust: { resize: false}, target: $(document) },
                    position: { viewport: true },
                    show: { ready: true, event: false, delay: 0 },
                    hide: { event: false },
                    content: { button: true }
                }
            });
        });*/

        $('#forget-btn').on('click', function(){
            utils.saveToLocalStorage('username', '');
            $('#username').val('');
        });

        var user = utils.getFromLocalStorage('username') || '',
            pass_md5 = utils.getFromLocalStorage('password_md5'),
            form = $('#login-form'),
            userDiv = $('#user-div');

        // trying to prefill username from local storage
        $('#username').val(user);

        if (user == '') {
            userDiv.hide();
            form.show();
            $('#main_content').hide();
            
        } else {
            if (utils.getFromLocalStorage('logout_clicked') !== 1) {
                userDiv.show();
                $('#user').html(user || '');
                form.hide();
                //$('#status').html('<br><br>Checking status...').show();
                popup.checkLoggedStatus();
            } else {
                userDiv.hide();
                form.show();
                $('#username').trigger('focus');
            }
        }

        $(document).on('click', '.show-item .badge', popup.onBadgeClick);
        $(document).on('click', '.overlay-close-btn', popup.onMainContentOverlayCloseClick);
        $(document).on('click', '.episode-item', popup.onEpisodeItemClick);
        $(document).on('click', '.show-link', popup.onShowLinkClick);
        $(document).on('click', '.show-item .title.linked', popup.onLinkedShowTitleClick);
        $(document).on('click', '.search', popup.onSearchLinkClick);
        //$('.episodes-container').on('scroll', popup.onEpisodesContainerScroll);        

        $(document).on('submit', '.search-form', popup.onSearchFormSubmit);

        $(document).on('click', '.search-results-container .found-item', function(){
            var $item = $(this);
            //$item.toggleClass('expanded');
            $item.find('.found-item-settings').slideToggle(function(){
                $item.toggleClass('expanded');
            });
        });

        $(document).on('click', '.search-results-container .label', popup.onFoundItemLabelClick);
        
        $(document).on('click', '.search-results-container .my-shows-icon', function(e){
            e.stopPropagation();
            window.open('https://myshows.me/view/' + $(this).parents('.found-item:first').attr('data-id') + '/');
        });

        $(document).on('click', '.show-item .my-shows-icon', function(e){
            window.open('https://myshows.me/view/' + $(this).parents('.show-item:first').attr('showid') + '/');
        });

        $('#showslist').sortable({
            axis: 'y',
            //handle: '.sort-handler',
            stop: function(event, ui) {
                var sorted = [];
                $('#showslist .show-item').each(function(){
                    sorted.push(parseInt($(this).attr('showid')));
                });
                // let it be syncable between computers
                utils.storage.set('settings_shows_order', sorted, 1);
                utils.setCfg('settings_shows_order_type', 'manual');
                popup.showInfo(utils.i18n('show_order_has_been_saved'), 1);
            }
        });

        /*utils.showInfotip({
            element: $('.logout-link'),
            position: 'left',
            text: utils.i18n('logout'),
            raw: { position: { my: 'bottom right', at: 'top left', adjust: {x: 5, y: 5} } }
        });
        utils.showInfotip({
            element: $('.help-link'),
            position: 'left',
            text: utils.i18n('instructions'),
            raw: { position: { my: 'bottom right', at: 'top left', adjust: {x: 5, y: 5} } }
        });*/

        // Feedback link tooltip
        utils.showInfotip({
            element: $('.footer .links a.feedback'),
            position: 'top',
            text: utils.i18n('feedback_link'),
            forceShow: 1,
            raw: { style: { width: 200 }, position: { adjust: {x: -5} } }
        });

        // Search link tooltip
        utils.showInfotip({
            element: $('.footer .links a.search'),
            position: 'top',
            text: utils.i18n('search_link'),
            forceShow: 1,
            raw: { style: { width: 200 }, position: { adjust: {x: -10} } }
        });

        // Stats link tooltip
        utils.showInfotip({
            element: $('.footer a.stats'),
            position: 'top',
            text: utils.i18n('stats_link'),
            forceShow: 1,
            raw: { style: { width: 200 } }
        });

        // Options link tooltip
        utils.showInfotip({
            element: $('.footer .links a.options'),
            position: 'top',
            text: utils.i18n('options_link'),
            forceShow: 1,
            raw: { style: { width: 200 }, position: { adjust: {x: -10} } }
        });

        // Reload link tooltip
        utils.showInfotip({
            element: $('.reload-link'),
            position: 'bottom',
            text: utils.i18n('reload_link'),
            forceShow: 1,
            //raw: { style: { width: 150 }, position: { my: 'top right', at: 'bottom left', adjust: {x: 0} } }
            raw: { style: { width: 200 }, position: { adjust: {x: 0} } }
        });
    },

    logout: function() {
        utils.saveToLocalStorage('logout_clicked', 1);
        utils.saveToLocalStorage('password_md5', '');
        $('.main-content-overlay').each(function(){
            popup.closeMainContentOverlay( $(this).attr('id') );            
        });
        $('#main_content, #user-div').hide();
        $('#login-form').show();
        $('body').removeClass('logged');
    },

    serviceCallXhr: [],
    serviceCallsQueue: false,

    serviceCall: function(opts, delayMs) {
        if (popup.serviceCallsQueue === false)
            popup.serviceCallsQueue = new $.myQueue();
        // Default delay - 1 sec (if there are other requests in queue)
        if (typeof delayMs == 'undefined')
            delayMs = 1000;

        var defaults = {
            url: false,
            type: 'get',
            data: false,
            success: false,
            error: function(xhr, status, error) {
                popup.showLoader(0);
                popup.showErr(utils.i18n('cannot_reach_service'));
            }
        };

        opts = $.extend({}, defaults, opts);
        opts.url = 'https://api.myshows.me/' + opts.url;

        // Otherwise get non-cached data
        var queueLengthBeforeAdding = popup.serviceCallsQueue.length();

        var onStart = null;
        var onExec = null;
        /*
        var onStart = function() {
            popup.showLoader(1);
            popup.showMsg({msg: utils.i18n('loading_status'), close: 0, type: 'info'});
        };
        var onExec = function() {
            popup.showLoader(0);
            popup.hideMsg();
        };
        */
        
        popup.serviceCallsQueue.add(function(){
            var xhr = $.ajax(opts);
            popup.serviceCallXhr.push(xhr);
            return xhr;
        }, queueLengthBeforeAdding == 0 ? 0 : delayMs, onStart, onExec);
        
        popup.serviceCallsQueue.run();
    },

    abortServiceCalls: function() {
        // Stop queue 
        if (typeof serviceCallsQueue != 'undefined')
            popup.serviceCallsQueue.stop();
        // Abort XHR requests if any
        $.each(popup.serviceCallXhr, function(idx, xhr){
            xhr.abort();
        });
        popup.serviceCallXhr = [];
    },

    checkLoggedStatus: function(opts) {
        var username = utils.getFromLocalStorage('username'),
            password_md5 = utils.getFromLocalStorage('password_md5');
        $('#main_content').hide();
        popup.showLoader(1);
        popup.showInfo(utils.i18n('connecting'), 0);
        popup.serviceCall({
            url: 'profile/login',
            data: {
                login: username,
                password: password_md5
            },
            success: function(response) {

            },
            statusCode: {
                200: function(xhr) {
                    // logged in
                    popup.showLoader(0);
                    popup.hideMsg();
                    $('#user').html(username).attr('title', username);
                    $('#user-div').show();
                    $('#main_content').show();
                    $('#login-form').hide();
                    $('body').addClass('logged');
                    popup.loadShowsList();
                },
                403: function(xhr) {
                    // wrong credentials
                    popup.showLoader(0);
                    popup.hideMsg();
                    popup.showErr(utils.i18n('wrong_credentials'));
                },
                404: function(xhr) {
                    // empty data
                    popup.showLoader(0);
                    popup.hideMsg();
                    popup.showErr(utils.i18n('wrong_credentials'));
                }
            }

        });
    },

    onLoginFormSubmit: function(e){
        e.preventDefault();
        var form = $(this),
            user = $.trim($('#username').val()),
            password = $.trim($('#password').val()),
            password_md5 = md5(password),
            btn = $('#auth-btn');
        if (user == '' || password == '') {
            popup.showErr(utils.i18n('login_or_pass_empty'));
            return;
        }
        
        utils.saveToLocalStorage('username', user);
        utils.saveToLocalStorage('password_md5', password_md5);
        // allowing automatic status check
        utils.saveToLocalStorage('logout_clicked', 0);
        popup.showLoader(1);
        btn.attr('disabled', 'disabled')

        popup.serviceCall({
            url: 'profile/login',
            data: {
                login: user,
                password: password_md5
            },
            success: function(response) {

            },
            statusCode: {
                200: function(xhr) {
                    // logged in
                    popup.showLoader(0);
                    popup.hideMsg();
                    $('#user').html(user);
                    $('#user-div').show();
                    $('#main_content').show();
                    $('#login-form').hide();
                    $('body').addClass('logged');
                    popup.loadShowsList();
                },
                403: function(xhr) {
                    // wrong credentials
                    popup.showLoader(0);
                    popup.hideMsg();
                    popup.showErr(utils.i18n('wrong_credentials'));
                },
                404: function(xhr) {
                    // empty data
                    popup.showLoader(0);
                    popup.hideMsg();
                    popup.showErr(utils.i18n('wrong_credentials'));
                }
            },
            error: function() {
               popup.showLoader(0); 
               btn.removeAttr('disabled');
               popup.showErr(utils.i18n('cannot_reach_service'));
            }
        });
    },

    loadShowsList: function() {
        popup.abortServiceCalls();
        popup.showMsg({msg: utils.i18n('loading_status'), close: 0, type: 'info'});
        popup.showLoader(1);
        //popup.showInfo(utils.i18n('loading_shows_status'));
        var showslist = $('#showslist');
        showslist.html('<div class="shows-loader">'+utils.i18n('loading_shows_status')+'</div>');
        $('#shows-container').detach().appendTo('#main_content');
        
        var userShowsDfr = popup.getUserShows();
        var showsOrderDfr = utils.storage.get('settings_shows_order', 0, 1);

        $.when(userShowsDfr, showsOrderDfr).done(function(userShows, shows_order) {
            popup.showLoader(0);
            popup.hideMsg();

            // backward compatibility - this may be available in local non-syncable storage
            if (!shows_order) {
                var tmp = utils.getFromLocalStorage('settings_shows_order');
                if (tmp) {
                    shows_order = tmp;
                    utils.storage.set('settings_shows_order', shows_order, 1)
                }
            }

            // final check
            shows_order = shows_order || [];

            var settings_shows_order_type = utils.getCfg('settings_shows_order_type');

            showslist.empty();
            var myShowsIds = [], 
                myShows = [],
                myShowsSorted = [];

            $.each(userShows, function(id, item){
                if (item.watchStatus != 'watching' && item.watchStatus != 'finished')
                    return true;
                if (utils.getCfg('settings_shows_hide_finished') == 'yes') {
                    if (item.watchStatus == 'finished' || (item.watchStatus == 'watching' && item.totalEpisodes == item.watchedEpisodes))
                        return true;
                }
                myShows[id] = item;
                myShowsIds.push(parseInt(id));
                //myShowsId2Idx[id] = myShowsIds.length - 1;
            });

            // Sort order is the same as on the myshows.me
            //console.log(settings_shows_order_type)
            if (settings_shows_order_type == 'site') {
                shows_order = [];
            // Alphabetic sort order on Russian titles
            } else if (settings_shows_order_type == 'alphabetic') {
                shows_order = [];
                myShowsIds = myShowsIds.sort(function(id1, id2){
                    if (myShows[id1].ruTitle < myShows[id2].ruTitle)
                        return -1;
                    if (myShows[id1].ruTitle > myShows[id2].ruTitle)
                        return 1;
                    return 0;
                });
            }

            // Form sorted array with shows:
            // 1) from user settings
            $.each(shows_order, function(idx, id){
                var index = myShowsIds.indexOf(id);
                if (index != -1) {
                    myShowsSorted.push(myShows[id]);
                    myShowsIds[index] = 0;
                    //delete myShows[id];
                }
            });

            // 2) unknown items
            $.each(myShowsIds, function(idx, id){
                if (id)
                    myShowsSorted.push(myShows[id]);
            });

            // Display
            $.each(myShowsSorted, function(i, item){
                // Display show name
                var itemElm = $('#templates .show-item').clone();
                itemElm.attr('showid', item.showId);
                if (item.ruTitle) {
                    itemElm.find('.title').html(item.ruTitle);
                    itemElm.find('.secondary').html(item.title);
                } else {
                    itemElm.find('.title').html(item.title);
                    itemElm.find('.secondary').html('');
                }
                // get url asynchronously (if any)
                utils.storage.get('show_url_' + item.showId, 0, 1).done(function(url){
                    if (url !== false && url != '')
                        itemElm.find('.title').addClass('linked').attr('data-url', url);
                });
                itemElm.data('db_data', item);

                utils.showInfotip({
                    element: itemElm.find('.show-link'),
                    position: 'top',
                    text: utils.i18n('show_link_info')
                });

                utils.showInfotip({
                    element: itemElm.find('.my-shows-icon'),
                    position: 'top',
                    text: utils.i18n('open_show_at_myshows_me')
                });
                
                if (utils.getCfg('settings_show_tooltips') == 'yes') {
                    utils.showInfotip({
                        element: itemElm.find('.show-drag-info-tip'),
                        position: 'right',
                        text: utils.i18n('drag_to_sort_show_items'),
                        raw: { 
                            show: { delay: 0, event: 'mouseenter click' }
                        }
                    });

                    utils.showInfotip({
                        element: itemElm.find('.badge-info-tip'),
                        position: 'left',
                        text: utils.i18n('save_show_progress'),
                        raw: { 
                            style: { width: 200 },
                            show: { delay: 0, event: 'mouseenter click' }
                        }
                    });
                } else {
                    itemElm.find('.info-tip').remove();
                }

                showslist.append(itemElm);

                // Display episode last seen. Running this in the queue with delay 1 second,
                // to avoid too frequent requests,
                popup.displayEpisodeLastSeen(item.showId);
            });
        });
    },

    /*
        @return deferred promise
    */
    displayEpisodeLastSeen: function(showId) {
        var epDfr = $.Deferred(),
            ep,
            itemElm = $('#showslist .show-item[showid="'+showId+'"]');
        // 1) try to fetch from synced storage
        utils.storage.get('show_' + showId + '_progress', 0, 1).done(function(ep){
            if (ep !== false && ep != '') {
                epDfr.resolve(ep);
            } else {
                // 2) fetch from API
                var a = popup.getShowInfo(showId), 
                    b = popup.getEpisodesSeen(showId);
                //popup.loadShowsQueue.push([a, b]);
                $.when(a, b)
                    .done(function(showInfo, episodesSeen){
                        var data = {
                                s: 0,
                                e: 0
                            },
                            maxEpBySeason = {};
                        // get max season/episode seen
                        $.each(episodesSeen, function(id, item){
                            // trying to fix an issue from the customer's bug report
                            if (typeof showInfo.episodes[id] == 'undefined') {
                                console.info(showInfo.title + ': просмотренная серия ' + id + ' не обнаружена в списке серий; ' + JSON.stringify(item))
                                return true; // go to next
                            }

                            var s = showInfo.episodes[id].seasonNumber,
                                e = showInfo.episodes[id].episodeNumber;
                            if (data.s < s)
                                data.s = s;
                            maxEpBySeason[s] = maxEpBySeason[s] || 0;
                            if (maxEpBySeason[s] < e)
                                maxEpBySeason[s] = e;
                        });
                        data.e = maxEpBySeason[data.s];
                        utils.storage.set('show_' + showId + '_progress', data, 1);
                        epDfr.resolve(data);
                    })
                    .fail(function(){
                        epDfr.reject();
                    });
            }
        });

        // On badge data get
        epDfr.done(function(data){
            itemElm.find('.badge').html(data.s == 0 ? '<span class="glyphicon glyphicon-folder-open"></span>' : data.s + ' &ndash; ' + data.e);
        });
        epDfr.fail(function(){
            itemElm.find('.badge').html(utils.i18n('error'));
        });

        return epDfr.promise();
    },

    onBadgeClick: function(e) {
        var badge = $(this),
            db_data = badge.parents('.show-item:first').data('db_data'),
            overlay,
            episodes = $('#templates .episode-select').clone(),
            list = episodes.find('.episodes-list'),
            ep_container = episodes.find('.episodes-container');
        // Is loading?
        var isLoading = 0;
        $('#showslist .show-item .badge').each(function(){
            if ($.trim($(this).text()) == '...') {
                isLoading = 1;
                return false;
            }
        });
        if (isLoading) {
            utils.showTooltip({
                element: badge,
                text: utils.i18n('badge_denied_because_of_loading'),
                position: 'left',
                raw: { 
                    style: { width: 200 },
                    show: { event: 'mouseenter click', ready: true, delay: 0 }
                }
            });
            e.stopPropagation();
            return;
        }
        overlay = popup.openMainContentOverlay();
        list.attr('showid', db_data.showId);
        popup.showLoader(1);
        episodes.appendTo(overlay);
        var mainTitle = db_data.ruTitle;
        var secondaryTitle = db_data.title;
        if (!db_data.ruTitle) {
            mainTitle = db_data.title;
            secondaryTitle = '';
        }
        episodes.find('.title').html(mainTitle)
        episodes.find('.secondary').html(secondaryTitle);
        ep_container.height(overlay.height() - (ep_container.offset().top - overlay.offset().top));
        list.html( '<span class="episode-loader-msg">' + utils.i18n('episodes_loading_status') + '</span>' );

        popup.getShowInfo(db_data.showId).done(function(response){
            popup.showLoader(0);
            var maxSeasonNumber = 0,
                ep = {};
            $.each(response.episodes, function(id, item){
                if (typeof ep[item.seasonNumber] == 'undefined')
                    ep[item.seasonNumber] = [];
                ep[item.seasonNumber].push(item);
                if (maxSeasonNumber < item.seasonNumber)
                    maxSeasonNumber = item.seasonNumber;
            });
            list.empty();
            for (var i = 1; i <= maxSeasonNumber; i++) {
                list.append('<li class="season-header">'+utils.i18n('season')+' '+i+'</li>')
                var sorted = ep[i].sort(function(a, b){
                            if (a.episodeNumber < b.episodeNumber)
                                return -1;
                            if (a.episodeNumber > b.episodeNumber)
                                return 1;
                            return 0;
                        });
                $.each(sorted, function(idx, item){
                    var elm = $('#templates .episode-item').clone();
                    elm.data('db_data', item)
                        .attr('s', item.seasonNumber)
                        .attr('e', item.episodeNumber);
                    elm.find('.title').html('<strong>' + item.episodeNumber + '</strong> ' + (item.title == '' ? item.shortName : item.title));
                    list.append(elm);
                    // Tooltip with air date
                    utils.showInfotip({
                        element: elm,
                        position: 'top',
                        text: utils.i18n('air_date', $.trim(item.airDate) == '' ? utils.i18n('air_date_unknown') : item.airDate),
                        forceShow: 1
                    });
                });
            }
            // prepare on scroll behaviour
            var containerOffset = overlay.find('.episodes-container').offset().top;
            episodes.find('.season-header').each(function(){
                var thisSticky = $(this).wrap('<div class="followWrap" />');
                thisSticky.parent().height(thisSticky.outerHeight());
                //log(thisSticky.offset().top)
                $.data(thisSticky[0], 'pos', thisSticky.offset().top - containerOffset);
            });        
            episodes.find('.episodes-container').on('scroll', popup.onEpisodesContainerScroll);        

            // Scroll to episode last seen
            utils.storage.get('show_' + db_data.showId + '_progress', 0, 1).done(function(progress){
                if (progress !== false && typeof progress['s'] == 'number' && typeof progress['e'] == 'number') {
                    var item = list.find('.episode-item[s="'+progress.s+'"][e="'+progress.e+'"]');
                    item.addClass('last-seen');
                    ep_container.scrollTo(item, 500, {
                        offset: { top: -80 },
                        onAfter: function() {
                            //ep_container.trigger('scroll');
                        }
                    });                
                }
            });
        });
    },

    onMainContentOverlayCloseClick: function(e) {
        e.preventDefault();
        popup.showLoader(0);
        popup.closeMainContentOverlay( $(e.target).parents('.main-content-overlay:first').attr('id') );
    },

    onEpisodeItemClick: function() {
        var elm = $(this),
            db_data = elm.data('db_data'),
            showId = elm.parents('.episodes-list:first').attr('showid'),
            showItem = $('#main_content .show-item[showid="'+showId+'"]'),
            show_db_data = showItem.data('db_data'),
            lastSeasonSeen = db_data.seasonNumber,
            lastEpisodeSeen = db_data.episodeNumber;
        
        // Save to service
        var episodesSeenIds = [], 
            maxSeason = 0,
            episodesBySeason = {};
        // first, fetch show full info including episodes
        popup.getShowInfo(showId).done(function(showInfo){
            // First, make sure we have sorted list of episodes by season:
            // 1) get my own array with organized episodes
            $.each(showInfo.episodes, function(id, ep){
                episodesBySeason[ep.seasonNumber] = episodesBySeason[ep.seasonNumber] || [];
                episodesBySeason[ep.seasonNumber].push(ep);
                if (maxSeason < ep.seasonNumber)
                    maxSeason = ep.seasonNumber;
            });
            // 2) get list of ids of the episodes seen
            for (var i = 1; i <= lastSeasonSeen; i++) {
                // make sure episodes are sorted by number
                var sorted = episodesBySeason[i].sort(function(a, b){
                        if (a.episodeNumber < b.episodeNumber)
                            return -1;
                        if (a.episodeNumber > b.episodeNumber)
                            return 1;
                        return 0;
                    });
                $.each(sorted, function(idx, ep){
                    if (i == lastSeasonSeen && ep.episodeNumber > lastEpisodeSeen)
                        return false; // exit from $.each when last episode seen found
                    episodesSeenIds.push(ep.id);
                });
            }
            // Save to service
            //popup.showLoader(1);
            popup.showPageLoader(1, utils.i18n('saving_status'), 1);
            popup.serviceCall({
                url: 'profile/shows/'+showId+'/sync?episodes='+episodesSeenIds.join(','),
                success: function(response) {
                    // Update UI
                    showItem.find('.badge').html(lastSeasonSeen + ' &ndash; ' + lastEpisodeSeen);
                    // Save to cache
                    utils.storage.set('show_' + showId + '_progress', {s: lastSeasonSeen, e: lastEpisodeSeen}, 1);
                    popup.closeMainContentOverlay( elm.parents('.main-content-overlay:first').attr('id') );
                    //popup.showLoader(0);
                    popup.showPageLoader(0);
                    popup.showInfo(utils.i18n('show_progress_saved', [show_db_data.ruTitle, lastSeasonSeen, lastEpisodeSeen]), 1);
                },
                statusCode: {
                    401: function(xhr) {
                        // need re-auth
                        //popup.showLoader(0);
                        popup.showPageLoader(0);
                        popup.on401();
                    }
                }
            });
        });
    },

    onSearchLinkClick: function(e) {
        e.preventDefault();

        if ($('.search-wrap:visible').length > 0)
            return;

        var elm = $(this),
            search = $('#templates .search-wrap').clone(),
            overlay,
            rs_container = search.find('.search-results-container');
        
        overlay = popup.openMainContentOverlay();
        search.appendTo(overlay);
        search.find('.title').html(utils.i18n('search_frame_heading'))
        rs_container.height(overlay.height() - (rs_container.offset().top - overlay.offset().top));
        search.find('form input').trigger('focus');
    },

    onSearchFormSubmit: function(e) {
        e.preventDefault();
        var form = $(this),
            search = form.parents('.search-wrap:first'),
            rsContainer = search.find('.search-results-container'),
            searchDfr = $.Deferred();

        popup.showLoader(1);
        //rsContainer.html(utils.i18n('search_status'));

        var q = $.trim(form.find('[name="q"]').val());
        if (q == '')
            return;

        var showsDfr = popup.getUserShows({returnType: 'statusById'});

        popup.abortServiceCalls();

        popup.serviceCall({
            url: 'shows/search/?q=' + encodeURIComponent(q),
            success: function(response) {
                popup.showLoader(0);
                response = JSON.parse(response);
                //console.log(response);
                searchDfr.resolve(response);
            },
            statusCode: {
                401: function(xhr) {
                    // need re-auth
                    popup.showLoader(0);
                    popup.hideMsg();
                    popup.on401();
                }
            },
            error: function() {
                popup.showLoader(0);
                popup.hideMsg();
                popup.showErr(utils.i18n('cannot_reach_service'));                   
            }
        });

        //
        $.when(showsDfr, searchDfr).done(function(userShows, showsFound){
            rsContainer.empty();
            $.each(showsFound, function(idx, show){
                var elm = $('#templates .found-item').clone(),
                    title = show.ruTitle,
                    sec = show.title;
                if (!title) {
                    title = sec;
                    sec = '';
                }
                elm.find('.title-in').html(title);
                elm.find('.secondary').html(sec);
                elm.attr('data-id', show.id);
                // status
                var status = typeof userShows[show.id] != 'undefined' ? userShows[show.id].watchStatus : 'remove';
                elm.find('[data-status="' + status + '"]').addClass('active-label');
                // tooltip
                utils.showInfotip({
                    element: elm.find('.my-shows-icon'),
                    position: 'left',
                    text: utils.i18n('open_show_at_myshows_me'),
                    raw: { style: { width: 200 }, position: { adjust: {x: 0} } }
                });

                elm.appendTo(rsContainer);
            });
            rsContainer.scrollTo(0, 500);
        });
    },

    onFoundItemLabelClick: function(e){
        e.stopPropagation();

        // watching, later, cancelled, remove 

        var $item = $(this),
            status = $item.attr('data-status'),
            $showItem = $item.parents('.found-item'),
            $labels = $showItem.find('.found-item-labels .label');

        if ($item.hasClass('disabled-label'))
            return;

        popup.showLoader(1);
        $labels.addClass('disabled-label');

        popup.serviceCall({
            url: 'profile/shows/' + $showItem.attr('data-id') + '/' + status,
            success: function(response) {
                popup.showLoader(0);
                $labels.removeClass('active-label')
                    .removeClass('disabled-label');
                $showItem.find('[data-status="' + status + '"]').addClass('active-label');
            },
            statusCode: {
                401: function(xhr) {
                    // need re-auth
                    popup.showLoader(0);
                    popup.hideMsg();
                    popup.on401();
                }
            },
            error: function() {
                popup.showLoader(0);
                popup.hideMsg();
                popup.showErr(utils.i18n('cannot_reach_service'));                   
            }
        });
    },

    onShowLinkClick: function() {
        var elm = $(this),
            show_item = elm.parents('.show-item:first'),
            db_data = show_item.data('db_data'),
            form = $('#templates .show-link-form').clone();

        //alert(JSON.stringify(db_data));
        form.find('.show-title').html(db_data.ruTitle);
        form.find('[name="show_url"]').val(show_item.find('.title').attr('data-url'));

        form.data('show_data', db_data).on('submit', popup.onShowLinkFormSubmit)
        form.find('.cancel-btn').on('click', function(){
            show_item.qtip('hide');
        });

        utils.showTooltip({
            element: show_item,
            position: 'center',
            text: form,
            type: 'help',
            raw: {
                show: { ready: true, event: false, delay: 0, solo: 1 },
                hide: { event: false },
                content: { button: 0 }
                //style: { classes: '' }
            }
        });
    },

    onShowLinkFormSubmit: function(e) {
        e.preventDefault();
        
        var form = $(this),
            url = $.trim(form.find('[name="show_url"]').val()),
            show_data = form.data('show_data'),
            show_item = $('#showslist .show-item[showid=' + show_data.showId + ']'),
            cache_key = 'show_url_' + show_data.showId;

        // if empty - delete
        if (url == '') {
            utils.storage.remove(cache_key, 1);
            show_item.find('.title').removeClass('linked').removeAttr('data-url');
            show_item.qtip('api').hide();
        } else {
            if (!/^https?:\/\//i.test(url))
                url = 'http://' + url;
            // save url into chrome.storage.sync
            utils.storage.set(cache_key, url, 1)
                .done(function(){
                    show_item.find('.title').addClass('linked').attr('data-url', url);
                    show_item.qtip('api').hide();
                })
                .fail(function(error){
                    utils.showTooltip({
                        element: form.find('[name=show_url]'),
                        position: 'top',
                        text: utils.i18n('cannot_save_url_error', [error]),
                        type: 'error'
                    });
                });
        }
    },

    onLinkedShowTitleClick: function(e) {
        e.stopPropagation();
        chrome.tabs.create({'url': $(this).attr('data-url')}, function(tab) {
            // Tab opened.
        });     
    },

    episodesHeaders: false,
    episodesHeadersShowId: 0,

    onEpisodesContainerScroll: function() {
        // Borrowed from http://codepen.io/chrissp26/pen/gBrdo
        var container = $(this),
            showId = parseInt(container.find('.episodes-list').attr('showid')),
            containerOffset = container.offset().top;
        if (popup.episodesHeaders === false || popup.episodesHeadersShowId != showId) {
            popup.episodesHeaders = container.find('.season-header');
        }
        popup.episodesHeaders.each(function(i){             
            var thisSticky = $(this),
                nextSticky = popup.episodesHeaders.eq(i+1),
                prevSticky = popup.episodesHeaders.eq(i-1),
                pos = $.data(thisSticky[0], 'pos');

            if (pos <= container.scrollTop()) {
                thisSticky
                    .addClass("fixed")
                    .css({top: containerOffset});
                
                if (nextSticky.length > 0 && thisSticky.offset().top - containerOffset >= $.data(nextSticky[0], 'pos') - thisSticky.outerHeight()) {
                    thisSticky
                        .addClass("absolute")
                        .css("top", $.data(nextSticky[0], 'pos') - thisSticky.outerHeight());
                }
            
            } else {
                thisSticky
                    .removeClass("fixed")
                    .removeAttr("style");
                
                if (prevSticky.length > 0 && $(container).scrollTop() <= $.data(thisSticky[0], 'pos')  - prevSticky.outerHeight()) {
                    prevSticky
                        .removeClass("absolute");
                }
            }
        });        
    },

    openMainContentOverlay: function() {
        var c = $('.content'),
            o = $('#templates .main-content-overlay').clone().attr('id', 'id' + ('' + Math.random()).replace(/\D/g, '')),
            m = $('#main_content'),
            mO = m.offset(),
            cO = c.offset(),
            h = m.height(),
            overlaysCount = c.find('.main-content-overlay').length;
        m.addClass('overlayed');
        o.appendTo(c)
            .css({
                top: mO.top - cO.top,
                height: h,
                'z-index': overlaysCount + 1
            });
        return o;
    },

    /*
        Cached for 6 hours
    */
    getShowInfo: function(id) {
        var dfr = $.Deferred(),
            data;
        // 1) try to fetch from cache
        data = utils.getFromLocalStorage('show_'+id, 3600*6);
        if (data !== false) {
            dfr.resolve(data);
        // 2) from API
        } else {
            popup.serviceCall({
                url: 'shows/'+id,
                success: function(response) {
                    try {
                        response = JSON.parse(response);
                    } catch (err) {
                        dfr.reject(err);
                        return;
                    }
                    utils.saveToLocalStorage('show_'+id, response);
                    dfr.resolve(response);
                },
                statusCode: {
                    401: function(xhr) {
                        // need re-auth
                        popup.on401();
                        dfr.reject();
                    }
                },
                error: function() {
                    dfr.reject();
                }
            });
        }
        return dfr.promise();
    },

    getEpisodesSeen: function(showId, delaySeconds) {
        delaySeconds = delaySeconds || 0;
        var dfr = $.Deferred(),
            data;
        // 1) try to fetch from cache
        //data = utils.getFromLocalStorage('episodes_seen_show_'+showId, 3600*6);
        //if (data !== false) {
        //    dfr.resolve(data);
        // 2) from API
        //} else {
            popup.serviceCall({
                url: 'profile/shows/'+showId+'/',
                success: function(response) {
                    try {
                        response = JSON.parse(response);
                    } catch (err) {
                        dfr.reject(err);
                        return;
                    }
                    utils.saveToLocalStorage('episodes_seen_show_'+showId, response);
                    dfr.resolve(response);
                },
                statusCode: {
                    401: function(xhr) {
                        // need re-auth
                        popup.on401();
                        dfr.reject();
                    }
                },
                error: function() {
                    dfr.reject();
                }
            });
        //}
        return dfr.promise();
    },

    closeMainContentOverlay: function(id) {
        var c = $('.content'),
            m = $('#main_content');
        c.find('.main-content-overlay#' + id).remove();
        if (c.find('.main-content-overlay').length == 0)
            m.removeClass('overlayed');
    },

    showPageLoader: function(isShow, msg, spin) {
        var overlay = $('.page-overlay'),
            modal = $('.page-modal'),
            w = $(window),
            H = w.height(),
            W = w.width(),
            w = modal.width(),
            h = modal.height();
        msg = msg || '';
        spin = spin || 0;
        modal.find('.msg').html(msg);
        if (isShow) {
            overlay.show();
            modal
                .css({
                    top: (H - h) / 2,
                    left: (W - w) / 2 
                })
                .fadeIn(200, function(){
                });
            if (spin) {
                var spinner = new Spinner({
                    lines: 7,
                    length: 0,
                    width: 4,
                    radius: 12,
                    corners: 1.0,
                    rotate: 0,
                    trail: 68,
                    speed: 1.3,
                    left: 0,
                    top: 0
                });

                spinner.spin(modal.find('.page-loader-spinner').get(0));
            }
        } else {
            overlay.hide();
            modal.hide();
        }
    },

    on401: function() {
        $('#main_content, #user-div').hide();
        $('#login-form').show();
        popup.showInfo(utils.i18n('reauth_required'));
    },

    showMsg: function(opts) {
        /*
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Oh snap!</strong> <a href="#" class="alert-link">Change a few things up</a> and try submitting again.
        </div>
        */
        opts = $.extend({}, {
            msg: '', 
            type: 'info',
            close: 1
        }, opts);
        //console.log(opts.msg, opts.close)
        var msgbox = $('#msgbox');
        if (msgbox.length == 0) {
            $('body').append('<div id="msgbox" class=""><div class="inner"></div></div>');
            msgbox = $('#msgbox');
        }
        if (popup.msgTimeoutID !== false) {
            clearTimeout(popup.msgTimeoutID);
            popup.msgTimeoutID = false;
        }
        msgbox.stop().find('.inner').html(opts.msg);
        msgbox
            .removeClass('msgbox-danger')
            .removeClass('msgbox-warning')
            .removeClass('msgbox-success')
            .removeClass('msgbox-info')
            .addClass('msgbox-'+opts.type)
            /*.css({
                left: ( $(window).width() - msgbox.width() ) / 2
            })*/
            .fadeIn('fast', function(){
                /*msgbox.css({
                    left: ( $(window).width() - msgbox.width() ) / 2
                });*/
            });
            //.show()
            //.fadeIn('fast')
            /*.css({
                left: ($(window).width() - msgbox.width())/2
            })*/
        if (opts.close) {
            popup.msgTimeoutID = setTimeout(function(){$('#msgbox').fadeOut(200);}, 5000);
        }
    },

    hideMsg: function() {
        if (popup.msgTimeoutID !== false) {
            clearTimeout(popup.msgTimeoutID);
            popup.msgTimeoutID = false;
        }
        $('#msgbox').stop().hide();
    },

    showInfo: function(msg, close) {
        close = close || 0;
        popup.showMsg({msg: msg, close: close, type: 'info'});
    },

    showErr: function(err) {
        popup.showMsg({msg: err, close: 0, type: 'warning'});
    },

    showLoader: function(isShow) {
        if (popup.spinner === false) 
            popup.spinner = new Spinner({
                lines: 7,
                length: 0,
                width: 3,
                radius: 5,
                corners: 1.0,
                rotate: 0,
                trail: 68,
                speed: 1.3,
                left: 0,
                top: 0
            });

        isShow ? popup.spinner.spin($('#loader').get(0)) : popup.spinner.stop();
    },

    getUserShows: function(opts) {
        opts = opts || {};
        opts = $.extend({}, {
            // defaults
            returnType: 'raw' 
        }, opts);

        var dfr = $.Deferred();
        var shows = utils.getFromLocalStorage('shows_list', 24*3600);
        // 1) try to fetch from cache
        if (shows !== false) {
            dfr.resolve(shows);
        // 2) from API
        } else {
            popup.serviceCall({
                url: 'profile/shows/',
                success: function(response) {
                    response = JSON.parse(response);
                    utils.saveToLocalStorage('shows_list', response);
                    dfr.resolve(response);
                },
                statusCode: {
                    401: function(xhr) {
                        // need re-auth
                        dfr.reject('401');
                    }
                },
                error: function() {
                    dfr.reject('connect_error');
                }
            });
        }
        return dfr.promise();
    }


};


function test() {
    $.ajax({
        url: 'https://api.myshows.me/profile/login',
        method: 'get',
        data: {
            login: 'aaa',
            password: 'bbb'
        },
        success: function(data) {
            log(1)
        },
        statusCode: {
            200: function(xhr) {
                // logged in
                log(202)
            },
            403: function(xhr) {
                // wrong credentials
                log(403)
            },
            404: function(xhr) {
                // empty data
                log(404)
            }
        },

    });

}

$(function(){
    //log('loaded...')

    popup.init();

    $('#btn').on('click', test);
});
