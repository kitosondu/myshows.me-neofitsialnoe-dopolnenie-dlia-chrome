// google analytics
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-47959961-1']);
_gaq.push(['_trackPageview']);
(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    ga.src = 'https://ssl.google-analytics.com/ga.js'; // special for extension
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
