var opts = {

    init: function() {
        utils.translate();
        var form = $('#options_form');
        // init form
        form.on('submit', opts.onFormSubmit);
        // 1
        $('[name="settings_interface"]').val(utils.getCfg('settings_interface'));
        // 2
        $('[name="settings_shows_order_type"]').val(utils.getCfg('settings_shows_order_type'));
        // 3
        //var chk = $('#settings_show_tooltips');
        //utils.getCfg('settings_show_tooltips') ? chk.attr('checked', 'checked') : chk.removeAttr('checked');
        $('[name="settings_show_tooltips"]').val(utils.getCfg('settings_show_tooltips'));

        $('[name="settings_shows_hide_finished"]').val(utils.getCfg('settings_shows_hide_finished'));

        // init on submit
        form.find('select, :checkbox').on('change', function(){ form.trigger('submit'); });
        // show form
        form.fadeIn();

        // other settings
        $('.options-close-btn').on('click', function(){ window.close(); });

        $('#options-del-cache-btn').on('click', function(){
            utils.deleteCache();
            opts.showStatus();
        });
    },

    onFormSubmit: function(e) {
        e.preventDefault();

        utils.setCfg('settings_interface', $('[name="settings_interface"]').val());
        utils.setCfg('settings_show_tooltips', $('[name="settings_show_tooltips"]').val());
        utils.setCfg('settings_shows_order_type', $('[name="settings_shows_order_type"]').val());
        utils.setCfg('settings_shows_hide_finished', $('[name="settings_shows_hide_finished"]').val());

        opts.showStatus();
    },

    showStatus: function() {
        $('#status').stop().fadeIn('fast', function(){
            setTimeout(function(){$('#status').fadeOut();}, 1000);
        });
    }

};

$(function(){
    //log('loaded...')

    opts.init();

});
