var utils = {

    i18n: function() {
        return chrome.i18n.getMessage.apply(chrome.i18n.getMessage, Array.prototype.slice.call(arguments));
    },

    translate: function(msgId) {
        //return chrome.i18n.getMessage(msgId);
        //log($('i18n').length)
        $('i18n, .i18n').each(function(){
            var elm = $(this);
            elm.html(utils.i18n(elm.html()));
        });
        $('[title_i18n]').each(function(){
            var elm = $(this);
            elm.attr('title', utils.i18n(elm.attr('title_i18n')));
        });
        $('input[placeholder_i18n], textarea[placeholder_i18n]').each(function(){
            var elm = $(this);
            elm.attr('placeholder', utils.i18n(elm.attr('placeholder_i18n')));
        });
    },

    getFromLocalStorage: function(key, expireSeconds) {
        expireSeconds = expireSeconds || 0;
        //log('wtf', key, typeof expireSeconds)
        if (typeof localStorage[key] == 'undefined') {
            return false;
        } else {
            var val;
            try {
                val = JSON.parse(localStorage[key]);
            } catch (e) {
                if (DEBUG)
                    console.log(e);
                return false;
            }
            var cached_time = 0;
            if (typeof val == 'object' && typeof val['cached_time'] == 'number') {
                cached_time = val.cached_time;
                val = val.data;
            }
            if (expireSeconds == 0 || cached_time > 0 && (new Date()).getTime() - cached_time <= expireSeconds*1000)
                return val;
            else
                return false;
        }
    },

    saveToLocalStorage: function(key, val) {
        //console.log(JSON.stringify(val))
        var wrapper = {
            data: val,
            cached_time: (new Date()).getTime()
        }
        try {
            localStorage[key] = JSON.stringify(wrapper);
        } catch (e) {
            if (DEBUG)
                console.log(e);
        }
    },

    /*
        Wrapper for the chrome.storage, which provides ability to sync user data
        between computers, see https://developer.chrome.com/extensions/storage.
        REMEMBER: it is the async stuff.
    */
    storage: {
        /*
            If isSync is true'ish, data will be saved into chrome.storage.sync,
            otherwize chrome.storage.local will be used

            expireSeconds: 0 means forever
        */
        set: function(key, val, isSync) {
            var area = isSync ? chrome.storage.sync : chrome.storage.local;
            var dfr = $.Deferred();
            // use array, because it can be saved without serializing
            var data = {};
            data[key] = [
                val,                   // data
                (new Date()).getTime() // cached time
            ];
            area.set(data, function(){
                if (typeof chrome.runtime.lastError != 'undefined')
                    dfr.reject(chrome.runtime.lastError);
                else
                    dfr.resolve();
            });
            return dfr.promise();
        },

        get: function(key, expireSeconds, isSync) {
            var area = isSync ? chrome.storage.sync : chrome.storage.local;
            var dfr = $.Deferred();
            area.get(key, function(items){
                if (typeof chrome.runtime.lastError != 'undefined') {
                    dfr.reject(chrome.runtime.lastError);
                } else {
                    var wrapper = items[key], 
                        val, 
                        cached_time = 0;
                    if (typeof wrapper == 'object' && wrapper.length == 2) {
                        val = wrapper[0];
                        cached_time = wrapper[1];
                        if (expireSeconds == 0 || cached_time > 0 && (new Date()).getTime() - cached_time <= expireSeconds*1000)
                            dfr.resolve(val);
                        else
                            dfr.resolve(false);
                    } else {
                        dfr.resolve(false);
                    }
                }
                
            });
            return dfr.promise();
        },

        remove: function(key, isSync) {
            var area = isSync ? chrome.storage.sync : chrome.storage.local;
            var dfr = $.Deferred();
            area.remove(key, function(){
                if (typeof chrome.runtime.lastError != 'undefined')
                    dfr.reject(chrome.runtime.lastError);
                else
                    dfr.resolve();
            });
            return dfr.promise();
        }
    },

    /*
     Improved version of parseInt
     */
    intVal: function(v) {
        v = parseInt(('' + v).replace(/\D/g, '').replace(/^0/g, ''));
        return isNaN(v) ? 0 : v;
    },

    pad: function(v) {
        return utils.intVal(v) < 10 ? '0'+v : v;
    },

    getCfg: function(name) {
        var defaults = {
            settings_interface: 'default',
            settings_show_tooltips: 'yes',
            settings_shows_order_type: 'site',
            settings_shows_hide_finished: 'no'
        };
        
        if (typeof defaults[name] != 'undefined') {
            var v = utils.getFromLocalStorage(name);
            return v === false ? defaults[name] : v;
        } else {
            return false;
        }
    },

    setCfg: function(name, value) {
        utils.saveToLocalStorage(name, value)
    },

    // TODO: apply
    getCfgSynced: function(name) {
        var dfr = $.Deferred();

        var defaults = {
            settings_interface: 'default',
            settings_show_tooltips: 'yes',
            settings_shows_order_type: 'site',
            settings_shows_hide_finished: 'no'
        };
        
        if (typeof defaults[name] != 'undefined') {
            utils.storage.get('config_' + name, 0, 1).done(function(val){
                val === false ? defaults[name] : val;
                dfr.resolve(val);
            });
        } else {
            dfr.resolve(false);
        }

        return dfr.promise();
    },

    // TODO: apply
    setCfgSynced: function(name, value) {
        return utils.storage.set('config_' + name, value, 1);
    },

    /*
     Displays tooltip based on qtip2.
     Supports following options:
     - content: see qtip2 docs
     - element: selector or object - tooltip target
     - position: left | right | top | bottom | center
     - positionAdjust: object. Example: {x: 10, y: -20}
     - type: 'error', 'info', 'help'
     - hide: string - event on hide. Default is 'unfocus'. Can be set to false if tooltip must be closed by button.
     - raw: object with any settings from documentation (http://craigsworks.com/projects/qtip2/docs/).
     It will merge and override any settings from other parameters.
     
     */
    showTooltip: function(opts) {
        var pos = {
            top: {my: 'bottom center', at: 'top center'},
            bottom: {my: 'top center', at: 'bottom center'},
            left: {my: 'right center', at: 'left center'},
            right: {my: 'left center', at: 'right center'},
            center: {my: 'center', at: 'center'}
        };
        var type2style = {
            error: 'qtip-red',
            info: 'qtip-dark',
            help: 'qtip-bootstrap'
        };
        var defaults = {
            position: 'top',
            element: $('body'),
            type: 'info',
            text: 'Info',
            raw: { content: { text: '' } }
        };

        // merge recursively
        opts = $.extend(true, defaults, opts);
        // text
        opts.raw['content']['text'] = opts.text;

        // not showing on hidden elements
        //if (!$(opts.element).is(':visible'))
        //    return false;

        var qtipOpts = {
            position: {
                my: pos[opts.position].my,
                at: pos[opts.position].at,
                viewport: $(window)
            },
            style: {
                classes: (typeof type2style[opts.type] == 'undefined' ? 'qtip-dark' : type2style[opts.type]) + ' qtip-rounded qtip-shadow qtip-my-' + opts.type
            }
        };
        qtipOpts = $.extend(true, qtipOpts, opts.raw);
        //console.log(qtipOpts);
        $(opts.element).qtip(qtipOpts);
    },

    showInfotip: function(opts) {
        var defaults = {
            position: 'top',
            element: $('body'),
            forceShow: 0,
            raw: {
                content: { text: '' },
                show: { solo: true, delay: 1000 },
                hide: { delay: 0 },
                events: {
                    show: function(event, api) {
                        if (utils.getCfg('settings_show_tooltips') == 'no')
                            event.preventDefault();
                    }
                }
            }
        };
        // merge recursively
        opts = $.extend(true, defaults, opts);
        if (opts.forceShow)
            opts.raw.events.show = {};
        //console.log(1, opts)
        utils.showTooltip(opts);
    },

    hideTooltips: function(type) {
        $(type == 'all' || typeof type == 'undefined' ? '.ui-tooltip, .qtip' : '.ui-tooltip-my-' + type).qtip('destroy');
    },

    openSettings: function() {
        chrome.tabs.create({url: 'options.html'});
    },

    deleteCache: function() {
        var key, 
            keysToDel = [];
        for (key in localStorage) {
            if (/^[a-z0-9]{32}$/.test(key) // md5
                || /^episodes_seen_show_\d+$/.test(key)
                || /^show_\d+(_progress)?$/.test(key)
                || /^shows_list$/.test(key)) 
            {
                keysToDel.push(key);
            }
        }
        $.each(keysToDel, function(idx, key){
            localStorage.removeItem(key);
        });
    }

};

function log(v) {
    if (typeof chrome != 'undefined')
        chrome.extension.getBackgroundPage().console.log(Array.prototype.slice.call(arguments));
    else
        console.log(Array.prototype.slice.call(arguments));
}
